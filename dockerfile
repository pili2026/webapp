FROM golang:1.20
WORKDIR /app
COPY . .
RUN go get -u github.com/gin-gonic/gin
RUN go build -o main .
EXPOSE 8888
CMD ["/app/main"]
